//bank some good vibe options
const vibes = [
  "...and you are awsome!",
  "...have a wonderful day!",
  "...and you've got this!",
  "...and so is this puppy!"
];

//choose a vibe at random
var vibe = vibes[Math.floor(Math.random() * Math.floor(vibes.length))];

//display a good vibe
document.querySelector(".vibe").append(vibe);