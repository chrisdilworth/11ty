//https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/getcurrentposition

function success(pos) {

  const WEATHER_API_KEY = "60c771dfb4b956532bed1075414e4a4c";
  const url = `https://api.openweathermap.org/data/2.5/weather?lat=${pos.coords.latitude}&lon=${pos.coords.longitude}&appid=${WEATHER_API_KEY}&units=metric`;

  fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      document.querySelector('#city').textContent = data.name;
      document.querySelector('#temp').textContent = data.main.temp +'C';
      document.querySelector('#main').textContent = data.weather[0].main;
      document.querySelector('#desc').textContent = data.weather[0].description;
      document.querySelector('#weather').classList.remove('hidden');
    })

}

function error(err) {
  console.warn(`ERROR(${err.code}): ${err.message}`);
}

if(navigator.geolocation){ //check that browser features work (Firefox doesn't)
  navigator.geolocation.getCurrentPosition(success, error);
}