module.exports = function(config){
  config.setBrowserSyncConfig({
    https: {
      key: 'C:/IDE2go/Git-Portable/mingw64/ssl/localhost.key',
      cert: 'C:/IDE2go/Git-Portable/mingw64/ssl/localhost.crt'
    }
  })
 
  config.addPassthroughCopy("src/js");
  return {
    dir: {
      input: "src",
      output: "dist",
      data: "_data"
    }
  };
};